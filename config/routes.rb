Rails.application.routes.draw do
  resources :services
  resources :profile
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'profile#index'

end
