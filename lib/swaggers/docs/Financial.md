# AccountManagementService::Financial

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_of_event** | **String** | Date of event for the update | [optional] 
**bsb** | **String** | BSB of the bank account | [optional] 
**account_number** | **String** | Account number of the bank account | [optional] 
**account_name** | **String** | Name of the person or entity who owns the account | [optional] 
**used_for_all_services** | **BOOLEAN** | TRUE | [optional] 


