# AccountManagementService::CCSApproval

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** | Service CCS Approval Status | [optional] 
**reason** | **String** | Reason associated with the status | [optional] 
**start_date** | **String** | Validity start date of this Service CCS Approval Status. | [optional] 
**end_date** | **String** | Validity end date of this Service CCS Approval Status. | [optional] 


