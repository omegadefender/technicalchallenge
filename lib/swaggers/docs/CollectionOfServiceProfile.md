# AccountManagementService::CollectionOfServiceProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ServiceProfile&gt;**](ServiceProfile.md) |  | [optional] 


