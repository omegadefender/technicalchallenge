# AccountManagementService::SeriousIncident

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notification** | **String** | Notification of serious incident | [optional] 
**steps_taken** | **String** | Steps taken to manage the serious incident | [optional] 
**date_of_serious_incident** | **String** | Date of the serious incident | [optional] 


