# AccountManagementService::RelatedCollectionAccountManagementRecordsToBeProvided

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;RecordsToBeProvided&gt;**](RecordsToBeProvided.md) |  | [optional] 


