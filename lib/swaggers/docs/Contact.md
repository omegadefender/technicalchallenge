# AccountManagementService::Contact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_of_event** | **String** | Date of event for the update | [optional] 
**phone** | **String** | Telephone | [optional] 
**mobile** | **String** | Mobile | [optional] 
**email** | **String** | Email address | [optional] 
**service_url** | **String** | Website of the service | [optional] 
**email_for_all_service** | **BOOLEAN** | TRUE | [optional] 


