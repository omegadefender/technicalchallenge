# AccountManagementService::ServiceProfilesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**serviceprofiles_get**](ServiceProfilesApi.md#serviceprofiles_get) | **GET** /serviceprofiles | Get entities from ServiceProfiles
[**serviceprofiles_post**](ServiceProfilesApi.md#serviceprofiles_post) | **POST** /serviceprofiles | Update ServiceProfile&#39;s entities
[**serviceprofiles_service_id_get**](ServiceProfilesApi.md#serviceprofiles_service_id_get) | **GET** /serviceprofiles({serviceID}) | Get entity from ServiceProfiles by key


# **serviceprofiles_get**
> Wrapper serviceprofiles_get(opts)

Get entities from ServiceProfiles

### Example
```ruby
# load the gem
require 'Account_Management_Service'

api_instance = AccountManagementService::ServiceProfilesApi.new

opts = { 
  filter: 'filter_example', # String | Filter items by following property values,        1. serviceID     2. startDate     3. endDate     
  expand: ['expand_example'] # Array<String> | Expand related entities   -  ChildCarePlaces   -  ServiceName   -  CCSApproval   -  Address   -  Contact   -  Financial   -  Fees   -  Trustee   -  ExternalManagement   -  ExternalManagement/Address   -  ServiceLocationOfRecord   -  ServiceLocationOfRecord/Address   -  ServiceTemporarilyCeasing   -  SupportingDocuments   -  ApprovalConditions   -  ApprovalConditions/Personnels   -  ApprovalConditions/RecordsToBeProvided   -  ACCSCapPercentages
}

begin
  #Get entities from ServiceProfiles
  result = api_instance.serviceprofiles_get(opts)
  p result
rescue AccountManagementService::ApiError => e
  puts "Exception when calling ServiceProfilesApi->serviceprofiles_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter items by following property values,        1. serviceID     2. startDate     3. endDate      | [optional] 
 **expand** | [**Array&lt;String&gt;**](String.md)| Expand related entities   -  ChildCarePlaces   -  ServiceName   -  CCSApproval   -  Address   -  Contact   -  Financial   -  Fees   -  Trustee   -  ExternalManagement   -  ExternalManagement/Address   -  ServiceLocationOfRecord   -  ServiceLocationOfRecord/Address   -  ServiceTemporarilyCeasing   -  SupportingDocuments   -  ApprovalConditions   -  ApprovalConditions/Personnels   -  ApprovalConditions/RecordsToBeProvided   -  ACCSCapPercentages | [optional] 

### Return type

[**Wrapper**](Wrapper.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **serviceprofiles_post**
> CreatedServiceProfile serviceprofiles_post(opts)

Update ServiceProfile's entities

### Example
```ruby
# load the gem
require 'Account_Management_Service'

api_instance = AccountManagementService::ServiceProfilesApi.new

opts = { 
  service_profile: AccountManagementService::ServiceProfile.new # ServiceProfile | New entity
}

begin
  #Update ServiceProfile's entities
  result = api_instance.serviceprofiles_post(opts)
  p result
rescue AccountManagementService::ApiError => e
  puts "Exception when calling ServiceProfilesApi->serviceprofiles_post: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_profile** | [**ServiceProfile**](ServiceProfile.md)| New entity | [optional] 

### Return type

[**CreatedServiceProfile**](CreatedServiceProfile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **serviceprofiles_service_id_get**
> CreatedServiceProfile serviceprofiles_service_id_get(service_id, opts)

Get entity from ServiceProfiles by key

### Example
```ruby
# load the gem
require 'Account_Management_Service'

api_instance = AccountManagementService::ServiceProfilesApi.new

service_id = 'service_id_example' # String | Unique identifier of the Service in the system

opts = { 
  expand: ['expand_example'] # Array<String> | Expand related entities   -  ChildCarePlaces   -  ServiceName   -  CCSApproval   -  Address   -  Contact   -  Financial   -  Fees   -  Trustee   -  ExternalManagement   -  ExternalManagement/Address   -  ServiceLocationOfRecord   -  ServiceLocationOfRecord/Address   -  ServiceTemporarilyCeasing   -  SupportingDocuments   -  ApprovalConditions   -  ApprovalConditions/Personnels   -  ApprovalConditions/RecordsToBeProvided   -  ACCSCapPercentages
}

begin
  #Get entity from ServiceProfiles by key
  result = api_instance.serviceprofiles_service_id_get(service_id, opts)
  p result
rescue AccountManagementService::ApiError => e
  puts "Exception when calling ServiceProfilesApi->serviceprofiles_service_id_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_id** | **String**| Unique identifier of the Service in the system | 
 **expand** | [**Array&lt;String&gt;**](String.md)| Expand related entities   -  ChildCarePlaces   -  ServiceName   -  CCSApproval   -  Address   -  Contact   -  Financial   -  Fees   -  Trustee   -  ExternalManagement   -  ExternalManagement/Address   -  ServiceLocationOfRecord   -  ServiceLocationOfRecord/Address   -  ServiceTemporarilyCeasing   -  SupportingDocuments   -  ApprovalConditions   -  ApprovalConditions/Personnels   -  ApprovalConditions/RecordsToBeProvided   -  ACCSCapPercentages | [optional] 

### Return type

[**CreatedServiceProfile**](CreatedServiceProfile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



