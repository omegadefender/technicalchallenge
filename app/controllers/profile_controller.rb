class ProfileController < ApplicationController

    require './lib/swaggers/Account_Management_Service'

    def index  
        ccs_service_id = 'service_01'
        api_configuration = AccountManagementService::Configuration.new { |config| config.debugging = true }
        api_client = AccountManagementService::ApiClient.new(api_configuration)
        api = AccountManagementService::ServiceProfilesApi.new(api_client)
        @service_profile = api.serviceprofiles_getlocal(ccs_service_id: ccs_service_id).d.results[0]
        @service_profile = @service_profile.to_hash()
    end
    
end
